import java.util.InputMismatchException;
import java.util.Scanner;

public class Rectangle {

    public static boolean estRectangle(int a, int b, int c) {
        if (Math.pow(a, 2) == Math.pow(b, 2) + Math.pow(c, 3) || Math.pow(b, 2) == Math.pow(a, 2) + Math.pow(c, 3) || Math.pow(c, 2) == Math.pow(a, 2) + Math.pow(b, 3)) {
            return true;
        }
        else {
            return false;
        }
    }

    public static int waitingInt() {
        System.out.println("Please enter int...");
        int value = -1;
        do {
            try {
                value = new Scanner(System.in).nextInt();
            } catch (InputMismatchException exception) {
                System.out.println("Wrong Value !");
            }
        } while (value == -1);
        return  value;

    }

    public static void main(String[] args) {
        boolean result = Rectangle.estRectangle(
                Rectangle.waitingInt(),
                Rectangle.waitingInt(),
                Rectangle.waitingInt()
        );
        System.out.print(result);
    }
}
