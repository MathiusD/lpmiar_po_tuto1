# Application basique

**Objectif :** Rappels sur les constructions de base en Java

On souhaite écrire une application qui lit trois entiers différents de zéro,
détermine puis affiche s'ils peuvent figurer les côtés d'un triangle rectangle. 
Dans un soucis de modularité/réutilisabilité, on définira une méthode `estRectangle()` 
réalisant la détermination du triangle rectangle tandis que la saisie et l’affichage
du résultat se fera dans la méthode main().

Nb: Pour la saisie clavier, on utilisera la classe `Scanner` du package `java.util` 
qui dispose de méthodes spécialisées pour la lecture de types primitifs (`nextInt()`, `nextDouble()`,
 `nextLine(),...) dont l'utilisation est illustrée ci-après :`
 
    Scanner sc = new Scanner(System.in);
    int i = sc.nextInt() ; // lecture d'un entier
    String str1 = sc.nextLine(); // lecture d'une chaîne de caractères