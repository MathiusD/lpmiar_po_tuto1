# La généricité

**Objectif :** Construire des classes à types paramétrés

Nous allons implanter une classe générique pour représenter des boîtes. 
Cette classe sera paramétrée par le type des objets à stocker dans la boîte.

Les boites auront une taille, et fonctionneront (un peu) comme des piles, c'est-à-dire qu'on pourra ajouter
des objets dans une boite et les retirer dans l'ordre dans lequel on les aura ajouter.

En interne les objets seront stockés dans un tableau.

1. Créez une classe `Boite<X>` générique munie des attributs adéquats.
2. Implémentez les méthodes suivantes :
    + un constructeur `Boite(int taille)` qui initialise le(s) attribut(s),
    + deux méthodes `vide()` / `pleine()` qui renvoie true si la boite est vide/pleine, et false sinon,
    + une méthode `ranger(X x)` qui ajouter l'élément au sommet de la boite, si c'est possible,
    + une méthode `X sortir()` qui retire le dernier élément ajouté et le retourne (si c'est possible)
    
Des cas de tests dans `TestBoite` vous permettent de valider votre développement.    