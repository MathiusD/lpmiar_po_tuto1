public class Boite<X> {

    X[] content;

    public Boite(int taille) {
        content = (X[]) new Object[taille];
    }

    public boolean vide() {
        for (X data: content)
            if (data != null)
                return false;
        return true;
    }

    public boolean pleine() {
        for (X data: content)
            if (data == null)
                return false;
        return true;
    }

    public boolean ranger(X x) {
        for (int index = 0; index < content.length; index++)
            if (content[index] == null) {
                content[index] = x;
                return true;
            }
        return false;
    }


    public X sortir() {
        if (this.vide() == true) return null;
        for (int index = content.length - 1; index >= 0; index--)
            if (content[index] != null) {
                X temp = content[index];
                content[index] = null;
                return temp;
            }
        return null;
    }

}
