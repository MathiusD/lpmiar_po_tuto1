import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestBoite {

    Boite<Chaussure> boite;

    @BeforeEach
    public void init() {
        boite = new Boite<>(2);
    }

    @Test
    public void test_vide() {
        assertAll(
                () -> assertTrue(boite.vide()),
                () -> assertFalse(boite.pleine())
        );
    }

    @Test
    public void test_videRetrait() {
        assertNull(boite.sortir());
    }

    @Test
    public void test1Ajout() {
        assertTrue(boite.ranger(new Chaussure()));
    }

    @Test
    public void test1Ajout_apres() {
        boite.ranger(new Chaussure());
        assertAll(
                () -> assertFalse(boite.vide()),
                () -> assertFalse(boite.pleine())
        );
    }

    @Test
    public void test2Ajouts_apres() {
        boite.ranger(new Chaussure());
        boite.ranger(new Chaussure());
        assertAll(
                () -> assertFalse(boite.vide()),
                () -> assertTrue(boite.pleine())
        );
    }

    @Test
    public void test2Ajouts() {
        boite.ranger(new Chaussure());
        assertTrue(boite.ranger(new Chaussure()));
    }

    @Test
    public void test3Ajouts() {
        boite.ranger(new Chaussure());
        boite.ranger(new Chaussure());
        assertFalse(boite.ranger(new Chaussure()));
    }


    @Test
    public void test1Ajout1Retrait() {
        Chaussure aRanger = new Chaussure();
        boite.ranger(aRanger);
        Chaussure rangee = boite.sortir();
        assertAll(
                () -> assertTrue(boite.vide()),
                () -> assertFalse(boite.pleine()),
                () -> assertEquals(aRanger, rangee)
        );
    }

    @Test
    public void test2Ajouts1Retrait() {
        Chaussure aRangerEn1 = new Chaussure();
        boite.ranger(aRangerEn1);
        Chaussure aRangerEn2 = new Chaussure();
        boite.ranger(aRangerEn2);
        Chaussure sortie1 = boite.sortir();
        assertAll(
                () -> assertFalse(boite.vide()),
                () -> assertFalse(boite.pleine()),
                () -> assertEquals(aRangerEn2, sortie1)
        );
    }

    @Test
    public void test2Ajouts2Retraits() {
        Chaussure aRangerEn1 = new Chaussure();
        boite.ranger(aRangerEn1);
        Chaussure aRangerEn2 = new Chaussure();
        boite.ranger(aRangerEn2);
        Chaussure sortie1 = boite.sortir();
        Chaussure sortie2 = boite.sortir();
        assertAll(
                () -> assertTrue(boite.vide()),
                () -> assertFalse(boite.pleine()),
                () -> assertEquals(aRangerEn2, sortie1),
                () -> assertEquals(aRangerEn1, sortie2)
        );
    }
}
