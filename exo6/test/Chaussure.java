public class Chaussure {

    static int NB_CHAUSSURE = 0;
    int numero;

    Chaussure() {
        numero = NB_CHAUSSURE++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Chaussure)) return false;

        Chaussure chaussure = (Chaussure) o;

        return numero == chaussure.numero;
    }

}
