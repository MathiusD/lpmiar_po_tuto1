# Chaînes de caractères

**Objectif :** Rappels sur les constructions de base en Java


Ecrire une méthode `palindrome()` qui accepte en paramètre une chaîne de caractères
et détermine si celle-ci est un palindrome. 

Un palindrome est une chaîne qui se lit indifféremment dans les deux sens de lecture.

Exemples :
+ "Laval"
+ "Tu l'as trop écrasé César ce Port-Salut"
+ "Élu par cette crapule"

Nb : on pourra utiliser la méthode d'instance `charAt(int index)` de la classe `String 
qui renvoie le caractère situé à la position index de la chaîne de caractères.
 
Nb2 : des cas de test Junit dans `TestPalindrome` vous permettent de valider votre implémentation.