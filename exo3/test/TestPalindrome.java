import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestPalindrome {

    @Test
    void test1() {
        assertTrue(Palindrome.palindrome(""));
    }

    @Test
    void test2() {
        assertTrue(Palindrome.palindrome("A"));
    }

    @Test
    void test3() {
        assertTrue(Palindrome.palindrome("xx"));
    }

    @Test
    void test4() {
        assertTrue(Palindrome.palindrome("laval"));
    }

    @Test
    void test41() {
        assertTrue(Palindrome.palindrome("Laval"));
    }

    @Test
    void test42() {
        assertFalse(Palindrome.palindrome("Lavable"));
    }

    @Test
    void test5() {
        assertTrue(Palindrome.palindrome("kayak"));
    }

    @Test
    void test6() {
        assertTrue(Palindrome.palindrome("ressasser"));
    }

    @Test
    void test61() {
        assertFalse(Palindrome.palindrome("ressassé"));
    }

    @Test
    void test10() {
        assertTrue(Palindrome.palindrome("Élu par cette crapule"));
    }

    @Test
    void test11() {
        assertTrue(Palindrome.palindrome("Tu l'as trop écrasé César ce Port-Salut"));
    }

    @Test
    void test12() {
        assertTrue(Palindrome.palindrome("À révéler mon nom, mon nom relèvera"));
    }

    @Test
    void test15() {
        assertTrue(Palindrome.palindrome("Eva, can I stab bats in a cave ?"));
    }

    @Test
    void test16() {
        assertFalse(Palindrome.palindrome("Eva, puis-je poignarder des chauves-souris dans une grotte ?"));
    }

    @Test
    void test18() {
        assertTrue(Palindrome.palindrome(" God ! A red nugget! A fat egg under a dog"));
    }

}
