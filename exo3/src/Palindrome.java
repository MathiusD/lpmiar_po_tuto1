import java.text.Normalizer;

public class Palindrome {

    public static boolean palindrome(String str) {
        String value = Palindrome.normalize(str);
        int midLength = value.length() / 2;
        boolean state = true;
        for (int index = 0; index < midLength && state == true; index++) {
            state = value.charAt(index) == value.charAt(value.length() - (index + 1));

        }
        return state;
    }

    public static String normalize(String str){
        return Normalizer.normalize(
                str.toLowerCase().replaceAll(" ", ""), Normalizer.Form.NFD
        ).replaceAll("[^\\p{ASCII}]", "").replaceAll("[^a-z^A-Z]", "");
    }
}
