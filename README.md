# Tutoriel n°1 : rappels Programmation Objet / Java

**@author :** Arnaud Lanoix Brauer (Arnaud.Lanoix@univ-nantes.fr)

D’après des exercices de **A. Queudet**  et **X. Aimé**

----
**IntelliJ à l'IUT**

A l'ouverture d'un nouveau projet, il peut être nécessaire de configurer correctement IntelliJ

1. Fixer dans `File > Project structure... > Project` le SDK Java : `/opt/app/JDK1_8xxxxx`.
2. Définir également le `Project language level` ainsi que le `Project Compiler output`.
3. Ajouter Junit 5 au projet : dans
 `Project structure... > Libraries > + > From Maven...`
chercher `org.junit.jupiter` ; 
Lancer la recherche en cliquant sur la loupe ; sélectionner 
`org.junit.jupiter:junit-jupiter:5.4.2` ; ok
