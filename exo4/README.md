# Objet `De6`

**Objectif :** La notion d'objet en Java

Écrire la classe `De6` qui doit regrouper les attributs et 
les constructeurs élémentaires permettant de créer un dé.
    + posé sur une face donnée ;
    + posé sur une face aléatoire.
    
Nb: la méthode `random()` de la classe `Math retourne un nombre au hasard entre 0 et 1. 

Munir cette classe d’accesseurs, d'une méthode `toString()`, etc.

Définir une méthode lancer() simulant le lancer du dé.

Ecrivez un programme `Prog1` qui devra créer un dé, effectuer 
un lancer puis afficher le résultat du lancer.

---

Écrivez un programme `Prog421`  qui lance trois dés plusieurs fois. 
Le nombre de lancers est passé en paramètre sur la ligne de commande.
Le résultat des dés est affiché à chaque lancer, 
et on dit si le joueur a gagné ou perdu (il gagne si on a les valeurs 4, 2 et 1).

---

Écrivez un programme `Prog36` où vous passer le nombre de joueurs en paramètres
 et qui simule un jeu de 36 (les enjeux seront fixés aléatoirement).

Jeu du 36 : Chaque joueur doit mettre un enjeu dans le pot. Puis, 
chacun lance le dé à son tour. Il annonce alors le résultat aux autres participant.
 Le joueur suivant joue à son tour et annonce le résultat précédemment réalisé 
par le premier joueur. Il faut atteindre 36 ou s’en rapprocher, sans le dépasser.
Si l’un des joueurs dépasse 36, il est éliminé. 
Le suivant joue en reprenant le résultat précédent. 
Le gagnant d’une partie de dés du trente-six est celui qui atteint 36 ou celui 
qui se rapproche le plus. Il gagne donc l’enjeu fixé en début de partie.