public class De6 {
    // TODO

    int face;

    public De6(int faceChoisie) {
        this.face = faceChoisie;
    }

    public De6() {
        this.lancer();
    }

    @Override
    public String toString() {
        return String.format("%d", this.face);
    }

    public void lancer() {
        this.face = (int)Math.round(1 + Math.random() * 5);
    }
}
