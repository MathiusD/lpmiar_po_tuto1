public class Prog421 {

    public static void main(String[] args) {
        int nbLanceDe = Integer.decode(args[0]);
        System.out.println(String.format("%d lancement par dé", nbLanceDe));
        for (int nbLancement = 0; nbLancement < 3; nbLancement++) {
            System.out.println(String.format("Dé %d", nbLancement + 1));
            De6 d = new De6();
            for (int nbLance = 0; nbLance < nbLanceDe; nbLance++) {
                System.out.println(String.format("Lancé %d", nbLance + 1));
                d.lancer();
                System.out.println(String.format("Résultat du dé : %s (%s)", d, d.face == 4 || d.face == 2 || d.face == 1 ? "Réussite" : "Echec"));
            }
        }
    }
}
