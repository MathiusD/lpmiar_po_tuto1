public class Prog36 {

    public static int dist(int value, int max){
        if (value > max){
            return  value - max;
        }
        else {
            return  max -value;
        }
    }

    public static void main(String[] args) {
        int max = 36;
        int nbPLayer = Integer.decode(args[0]);
        int[] players = new int[nbPLayer];
        System.out.println(String.format("%d joueurs", nbPLayer));
        De6 d = new De6();
        int gain =  d.face * nbPLayer;
        int comptFail = 0;
        int indexWinner = -1;
        int lastScore = 0;
        while (comptFail < nbPLayer && indexWinner == -1){
            comptFail = 0;
            for (int indexPlayer = 0; indexPlayer < players.length && (comptFail < nbPLayer && indexWinner == -1); indexPlayer++) {
                if (players[indexPlayer] <= max) {
                    d.lancer();
                    int val = d.face;
                    System.out.println(String.format("Player %d roll a %d !", indexPlayer + 1, val));
                    val += lastScore;
                    players[indexPlayer] = val;
                    System.out.println(String.format("Player %d has %d points !", indexPlayer + 1, val));
                    if (val == max){
                        indexWinner = indexPlayer;
                        System.out.println(String.format("Player %d has made a %s !", indexPlayer + 1, max));
                    }
                    if (players[indexPlayer] > max) {
                        System.out.println(String.format("Player %d are eliminated !", indexPlayer + 1));
                    } else {
                        lastScore = val;
                    }
                } else {
                    comptFail++;
                }
            }
        }
        if (comptFail == nbPLayer){
            indexWinner = -1;
            int bestValue = max;
            for (int indexPlayer = 0; indexPlayer < players.length; indexPlayer++) {
                if (Prog36.dist(players[indexPlayer], max) < bestValue) {
                    indexWinner = indexPlayer;
                }
            }
        }
        System.out.println(String.format("Winner is %d player, He won %d !", indexWinner + 1, gain));
    }
}
