# Héritage et polymorphisme en Java

**Objectif :** Utiliser l’héritage pour définir de nouvelles classes

Dans le cadre de l’informatisation d’une mairie, 
on veut automatiser le calcul des impôts locaux. 
On distingue deux catégories d’habitation : les habitations à usage professionnel 
et les maisons individuelles, l’impôt se calculant différemment selon le type d’habitation.

Définir une classe `Habitation` munies des attributs suivants :
+ le nom du propriétaire
+ l’adresse de l’habitation
+ la surface au sol de l’habitation permettant de calculer le montant de l’impôt.
Et de 2 méthodes :
+ `double impot()` qui permet de calculer le montant de l’impôt que doit payer
le propriétaire de l’habitation à raison de 0,35€ par m2.
+ `String toString()` qui permet d’afficher les trois attributs de la classe `Habitation`.

---

_Le calcul de l’impôt d’une maison individuelle est différent de celui d’une habitation,
 il se calcule en fonction de la surface habitable, du nombre de pièces et de la présence
 ou non d’une piscine. On compte 15€ supplémentaire par pièce et 75€ en cas de présence
 d’une piscine._
 
 
 Définir la classe `HabitationIndividuelle` qui hérite de la classe `Habitation`. 
 Ajouter les attributs adéquats.
 
 Redéfinir les méthodes` impot()` et `toString()`. 
 La méthode `toString() ` doit afficher, les attributs propriétaire, adresse et surface 
 de la classe `Habitation`, ainsi que les attributs propres à la classe `HabitationIndividuelle`.
 
 ---
 
 _Le calcul de l’impôt d’une habitation à usage professionnel est également différent de 
 celui d’une habitation. 
 Il se calcule en fonction de la surface au sol occupée par le bâtiment et
  du nombre d’employés travaillant dans l’entreprise. On compte 150€
 supplémentaire par tranche de 10 employés (ex : 0 → 10 : 150€, 11 → 20 : 300€, etc.)._
 
 Définir la classe `HabitationProfessionnelle` qui hérite de la classe
 Habitation. Ajouter le (ou les) attribut(s) nécessaire(s).
 6. Redéfinir les méthodes `impot()` et `toString()`.
 
 Des cas de tests Junit définis dans `TestHabitations` vous permettent de valider votre implémentation
 des méthodes `impot()`.
 
 ---
 
 On désire à présent calculer l’impôt local total des habitations
  (individuelles ou professionnelles) d’une commune. 
  Pour cela, définissez une classe` Commune` dans laquelle on pourra ajouter des habitations, 
  et calculer l'impôt.
  
  NB : les habitations seront stockées dans un tableau.

 Des cas de tests Junit définis dans `TestCommune` vous permettent de valider votre implémentation
 de la méthode méthode `impotTotal()`.