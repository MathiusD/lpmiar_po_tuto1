import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestHabitations {

    Habitation maison;

    @Test
    public void habitation() {
        maison = new Habitation("Toto", "Toto", 100);
        assertEquals(35.0, maison.impot(), 0.001);
    }

    @Test
    public void habitation2() {
        maison = new Habitation("Toto", "Toto", 2000);
        assertEquals(700, maison.impot(), 0.001);
    }

    @Test
    public void habitationIndividuelle() {
        maison = new HabitationIndividuelle("Toto", "Toto", 100, 3, false);
        assertEquals(80, maison.impot(), 0.001);
    }

    @Test
    public void habitationIndividuelle2() {
        maison = new HabitationIndividuelle("Toto", "Toto", 100, 3, true);
        assertEquals(155, maison.impot(), 0.001);
    }

    @Test
    public void habitationIndividuelle3() {
        maison = new HabitationIndividuelle("Toto", "Toto", 100, 3);
        assertEquals(80, maison.impot(), 0.001);
    }

    @Test
    public void habitationProfessionnelle0() {
        maison = new HabitationProfessionnelle("Toto", "Toto", 2000, 0);
        assertEquals(850, maison.impot(), 0.001);
    }

    @Test
    public void habitationProfessionnelle1() {
        maison = new HabitationProfessionnelle("Toto", "Toto", 2000, 1);
        assertEquals(850, maison.impot(), 0.001);
    }

    @Test
    public void habitationProfessionnelle2() {
        maison = new HabitationProfessionnelle("Toto", "Toto", 2000, 2);
        assertEquals(850, maison.impot(), 0.001);
    }

    @Test
    public void habitationProfessionnelle3() {
        maison = new HabitationProfessionnelle("Toto", "Toto", 2000, 3);
        assertEquals(850, maison.impot(), 0.001);
    }

    @Test
    public void habitationProfessionnelle10() {
        maison = new HabitationProfessionnelle("Toto", "Toto", 2000, 10);
        assertEquals(850, maison.impot(), 0.001);
    }

    @Test
    public void habitationProfessionnelle11() {
        maison = new HabitationProfessionnelle("Toto", "Toto", 2000, 11);
        assertEquals(1000, maison.impot(), 0.001);
    }

    @Test
    public void habitationProfessionnelle20() {
        maison = new HabitationProfessionnelle("Toto", "Toto", 2000, 20);
        assertEquals(1000, maison.impot(), 0.001);
    }

    @Test
    public void habitationProfessionnelle21() {
        maison = new HabitationProfessionnelle("Toto", "Toto", 2000, 21);
        assertEquals(1150, maison.impot(), 0.001);
    }

    @Test
    public void habitationProfessionnelle30() {
        maison = new HabitationProfessionnelle("Toto", "Toto", 2000, 30);
        assertEquals(1150, maison.impot(), 0.001);
    }
}
