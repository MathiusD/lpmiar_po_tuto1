import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCommune {

    Commune commune;

    @BeforeEach
    void reinit() {
        commune = new Commune();
    }

    @Test
    void communeVide() {
        assertEquals(0.0, commune.impotTotal(), 0.00001);
    }

    @Test
    void commune1habitation1() {
        commune.ajouter(new HabitationIndividuelle("Toto", "Toto", 100, 3, true));
        assertEquals(155.0, commune.impotTotal(), 0.00001);
    }

    @Test
    void commune1habitation2() {
        commune.ajouter(new HabitationIndividuelle("Toto", "Toto", 100, 3));
        assertEquals(80.0, commune.impotTotal(), 0.00001);
    }

    @Test
    void commune1habitation3() {
        commune.ajouter(new HabitationProfessionnelle("Toto", "Toto", 2000, 20));
        assertEquals(1000.0, commune.impotTotal(), 0.00001);
    }

    @Test
    void commune1habitation4() {
        commune.ajouter(new HabitationProfessionnelle("Toto", "Toto", 2000, 21));
        assertEquals(1150.0, commune.impotTotal(), 0.00001);
    }

    @Test
    void commune2habitations1() {
        commune.ajouter(new HabitationIndividuelle("Toto", "Toto", 100, 3, true));
        commune.ajouter(new HabitationProfessionnelle("Toto", "Toto", 2000, 20));
        assertEquals(1155.0, commune.impotTotal(), 0.00001);
    }

    @Test
    void commune2habitations2() {
        commune.ajouter(new HabitationProfessionnelle("Toto", "Toto", 2000, 21));
        commune.ajouter(new HabitationIndividuelle("Toto", "Toto", 100, 3, true));
        assertEquals(1305.0, commune.impotTotal(), 0.00001);
    }

    @Test
    void commune4habitations() {
        commune.ajouter(new HabitationProfessionnelle("Toto", "Toto", 2000, 21));
        commune.ajouter(new HabitationIndividuelle("Toto", "Toto", 100, 3));
        commune.ajouter(new HabitationIndividuelle("Toto", "Toto", 100, 3, true));
        commune.ajouter(new HabitationProfessionnelle("Toto", "Toto", 2000, 20));
        assertEquals(2385.0, commune.impotTotal(), 0.00001);
    }

}
