public class HabitationProfessionnelle extends Habitation {

    int nbEmployes;

    public HabitationProfessionnelle(String proprio, String adresse, double surface, int nbEmployes) {
        super(proprio, adresse, surface);
        this.nbEmployes = nbEmployes;
    }

    @Override
    public double impot() {
        int mod = this.nbEmployes / 10;
        if (this.nbEmployes % 10 != 0) mod += 1;
        if (mod == 0) mod = 1;
        return super.impot() + mod * 150;
    }

    @Override
    public String toString() {
        return String.format("%sNombre d'Employés : %s", super.toString(), this.nbEmployes);
    }
}
