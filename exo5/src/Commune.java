public class Commune {

    private Habitation[] habitations;

    public Commune() {
        habitations = new Habitation[0];
    }

    public void ajouter(Habitation maison) {
        Habitation[] temp = habitations;
        habitations = new Habitation[temp.length + 1];
        for (int index = 0; index < temp.length; index++)
            habitations[index] = temp[index];
        habitations[temp.length] = maison;
    }

    public double impotTotal() {
        double tot = 0;
        for (Habitation habitation: habitations)
            tot += habitation.impot();
        return tot;
    }


}
