public class Habitation {

    String proprio;
    String adresse;
    double surface;

    public Habitation(String proprio, String adresse, double surface) {
        this.proprio = proprio;
        this.adresse = adresse;
        this.surface = surface;
    }

    public Habitation() {
        // TODO : ce constructeur peut être effacé une fois qu'il n'est plus utilisé
    }


    public double impot() {
        return 0.35 * this.surface;
    }

    @Override
    public String toString() {
        return String.format("Proprio : %s\nAdresse : %s\nSurface : %d m²", this.proprio, this.adresse, this.surface);
    }
}

