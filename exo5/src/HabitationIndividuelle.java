public class HabitationIndividuelle extends Habitation {

    int nbPiece;
    boolean piscine;

    public HabitationIndividuelle(String proprio, String adresse, double surface, int nbPieces, boolean piscine) {
        super(proprio, adresse, surface);
        this.nbPiece = nbPieces;
        this.piscine = piscine;
    }

    public HabitationIndividuelle(String proprio, String adresse, double surface, int nbPieces) {
        this(proprio, adresse, surface, nbPieces, false);
    }

    @Override
    public double impot() {
        return super.impot() + (15 * this.nbPiece) + (this.piscine ? 75 : 0);
    }

    @Override
    public String toString() {
        return String.format("%sNombre de Pièces : %s\nPiscine : %s", super.toString(), this.nbPiece, this.piscine);
    }
}
