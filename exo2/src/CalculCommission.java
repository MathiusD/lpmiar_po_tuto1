import java.util.Scanner;

public class CalculCommission {

    public static double calculCommission(double[] articles, int nbArticles) {
        double comission = 200.0;
        for (int articleIndex = 0; articleIndex < nbArticles; articleIndex++){
            comission += 0.09 * articles[articleIndex];
        }
        return comission;
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter nbArticles");
        int nbArticles = scan.nextInt();
        double[] articles = new double[nbArticles];
        for (int articleIndex = 0; articleIndex < nbArticles; articleIndex++){
            System.out.println(String.format("Please enter article %d", articleIndex + 1));
            articles[articleIndex] = scan.nextInt();
        }
        System.out.println(CalculCommission.calculCommission(articles, nbArticles));
    }
}
