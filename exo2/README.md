# Tableaux

**Objectif :** Rappels sur les constructions de base en Java

Une grande société paie ses vendeurs sur la base d'une commission.
Les vendeurs reçoivent 200 € par semaine plus 9% de leurs ventes de la semaine. 

Par exemple, un vendeur qui vend pour 5000€ de marchandises sur une semaine 
reçoit 200 € plus 9% de 5000 €, soit un total de 650 €. 

La société dispose pour chaque vendeur d'un tableau des articles 
vendus la semaine passée. 

Celle-ci se répartit comme suit :

| Article | Valeur |
| ------ | ----------|
| 0 |  239,99€ | 
| 1 |  129,75€ | 
| 2 | 99,95€ | 
| 3 | 350,89€ | 

Développez une application Java qui entre les articles vendus par 
un vendeur sur la semaine précédente, 
puis calcule et affiche la commission du vendeur.
La saisie s’effectuera dans la fonction `main()` tandis 
que l’on définira une méthode dédiée `calculCommission()` 
pour le calcul de la commission.

NB : des cas de test Junit dans `TestCommission` vous permettent de valider 
votre implémentation de `calculCommission()`.