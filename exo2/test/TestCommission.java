import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCommission {

    double[] articles;

    @BeforeEach
    public void init() {
        articles = new double[10];
    }

    @Test
    public void test_vide() {
        assertEquals(200.00, CalculCommission.calculCommission(articles, 0), 0.01);
    }

    @Test
    public void test_1Article() {
        articles[0] = 5000.0;
        assertEquals(650.00, CalculCommission.calculCommission(articles, 1), 0.01);
    }

    @Test
    public void test_Articles() {
        articles = new double[]{239.99, 129.75, 99.75, 350.89};
        assertEquals(273.83, CalculCommission.calculCommission(articles, 4), 0.01);
    }
}
